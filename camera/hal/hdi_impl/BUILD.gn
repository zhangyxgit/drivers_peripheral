# Copyright (c) 2021 - 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("../camera.gni")

if (defined(ohos_lite)) {
  import("//build/ohos.gni")
  import("//drivers/hdf_core/adapter/uhdf/uhdf.gni")

  config("camhdi_impl_config") {
    visibility = [ ":*" ]
    cflags = [
      "-DGST_DISABLE_DEPRECATED",
      "-DHAVE_CONFIG_H",
    ]

    cflags_cc = [ "-std=c++17" ]
    ldflags = [ "-Wl" ]

    ldflags += [ "--coverage" ]
  }

  ohos_shared_library("camera_host_service_1.0") {
    output_extension = "z.so"
    sources = [
      "src/camera_device/camera_device_impl.cpp",
      "src/camera_host/camera_host_config.cpp",
      "src/camera_host/camera_host_impl.cpp",
      "src/camera_host/hcs_deal.cpp",
      "src/offline_stream_operator/offline_stream.cpp",
      "src/offline_stream_operator/offline_stream_operator.cpp",
      "src/stream_operator/capture_message.cpp",
      "src/stream_operator/capture_request.cpp",
      "src/stream_operator/stream_base.cpp",
      "src/stream_operator/stream_operator.cpp",
      "src/stream_operator/stream_post_view.cpp",
      "src/stream_operator/stream_preview.cpp",
      "src/stream_operator/stream_statistics.cpp",
      "src/stream_operator/stream_still_capture.cpp",
      "src/stream_operator/stream_tunnel/lite/stream_tunnel.cpp",
      "src/stream_operator/stream_video.cpp",
    ]

    include_dirs = [
      "$camera_path/../interfaces/include",
      "$camera_path/../interfaces/hdi_passthrough",
      "$camera_path/include",
      "$camera_path/hdi_impl",
      "$camera_path/utils/watchdog",
      "$camera_path/hdi_impl/include",
      "$camera_path/hdi_impl/include/camera_host",
      "$camera_path/hdi_impl/include/camera_device",
      "$camera_path/hdi_impl/include/stream_operator",
      "$camera_path/hdi_impl/src/stream_operator/stream_tunnel/lite",
      "$camera_path/hdi_impl/include/offline_stream_operator",
      "$camera_path/device_manager/include/",
      "$camera_path/buffer_manager/src/buffer_adapter/lite",
      "$camera_path/utils/event",
      "//base/hiviewdfx/interfaces/innerkits/libhilog/include",
      "//drivers/peripheral/display/interfaces/include",
      "//drivers/peripheral/base",

      #producer
      "//commonlibrary/utils_lite/include",
      "$camera_path/pipeline_core/utils",
      "$camera_path/pipeline_core/include",
      "$camera_path/pipeline_core/host_stream/include",
      "$camera_path/pipeline_core/nodes/include",
      "$camera_path/pipeline_core/nodes/src/node_base",
      "$camera_path/pipeline_core/nodes/src/dummy_node",
      "$camera_path/pipeline_core/pipeline_impl/include",
      "$camera_path/pipeline_core/pipeline_impl/src",
      "$camera_path/pipeline_core/pipeline_impl/src/builder",
      "$camera_path/pipeline_core/pipeline_impl/src/dispatcher",
      "$camera_path/pipeline_core/pipeline_impl/src/parser",
      "$camera_path/pipeline_core/pipeline_impl/src/strategy",
      "$camera_path/pipeline_core/ipp/include",

      "//system/core/include/cutils",
    ]

    deps = [
      "$camera_path/buffer_manager:camera_buffer_manager",
      "$camera_path/device_manager:camera_device_manager",
      "$camera_path/pipeline_core:camera_pipeline_core",
      "$camera_path/utils:camera_utils",
      "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
    ]
    external_deps = [
      "drivers_interface_camera:metadata",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_utils",
    ]
    public_configs = [ ":camhdi_impl_config" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_camera"
  }
} else {
  import("//build/ohos.gni")
  import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

  config("camhdi_impl_config") {
    visibility = [ ":*" ]
    cflags = [
      "-DGST_DISABLE_DEPRECATED",
      "-DHAVE_CONFIG_H",
    ]

    ldflags = [ "-Wl" ]

    if (enable_camera_device_utest) {
      cflags += [
        "-fprofile-arcs",
        "-ftest-coverage",
      ]

      ldflags += [ "--coverage" ]
    }
  }

  host_sources = [
    "$camera_path/../interfaces/hdi_ipc/camera_host_driver.cpp",

    #"$camera_path/../interfaces/hdi_ipc/utils/src/utils_data_stub.cpp",
    "$camera_path/hdi_impl/src/camera_device/camera_device_impl.cpp",
    "$camera_path/hdi_impl/src/camera_dump.cpp",
    "$camera_path/hdi_impl/src/camera_host/camera_host_config.cpp",
    "$camera_path/hdi_impl/src/camera_host/camera_host_impl.cpp",
    "$camera_path/hdi_impl/src/camera_host/hcs_deal.cpp",
    "$camera_path/hdi_impl/src/offline_stream_operator/offline_stream.cpp",
    "$camera_path/hdi_impl/src/offline_stream_operator/offline_stream_operator.cpp",
    "$camera_path/hdi_impl/src/stream_operator/capture_message.cpp",
    "$camera_path/hdi_impl/src/stream_operator/capture_request.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_base.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_operator.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_post_view.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_preview.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_statistics.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_still_capture.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_tunnel/standard/stream_tunnel.cpp",
    "$camera_path/hdi_impl/src/stream_operator/stream_video.cpp",
  ]

  host_includes = [
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/metadata_manager/include",
    "$camera_path/utils/watchdog",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/buffer_manager/src/buffer_adapter/standard",
    "//base/hiviewdfx/interfaces/innerkits/libhilog/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/display/interfaces/include",

    #producer
    "//foundation/communication/ipc/ipc/native/src/core/include",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/pipeline_core/nodes/src/node_base",
    "$camera_path/pipeline_core/nodes/src/dummy_node",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/pipeline_impl/src",
    "$camera_path/pipeline_core/pipeline_impl/src/builder",
    "$camera_path/pipeline_core/pipeline_impl/src/dispatcher",
    "$camera_path/pipeline_core/pipeline_impl/src/parser",
    "$camera_path/pipeline_core/pipeline_impl/src/strategy",
    "$camera_path/pipeline_core/ipp/include",

    # hcs parser
    "//system/core/include/cutils",
  ]

  ohos_shared_library("camera_host_service_1.0") {
    sources = host_sources
    include_dirs = host_includes

    deps = [
      "$camera_path/buffer_manager:camera_buffer_manager",
      "$camera_path/device_manager:camera_device_manager",
      "$camera_path/metadata_manager:camera_metadata_manager",
      "$camera_path/pipeline_core:camera_pipeline_core",
      "$camera_path/utils:camera_utils",
      "//drivers/hdf_core/adapter/uhdf2/host:libhdf_host",
      "//drivers/interface/camera/v1_0:libcamera_stub_1.0",
    ]

    defines = []
    if (enable_camera_device_utest) {
      defines += [ "CAMERA_DEVICE_UTEST" ]
    }
    if (use_hitrace) {
      defines += [ "HITRACE_LOG_ENABLED" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "graphic_chipsetsdk:surface",
        "hdf_core:libhdf_host",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
        "ipc:ipc_single",
      ]
      if (use_hitrace) {
        external_deps += [ "hitrace_native:libhitracechain" ]
      }
    } else {
      external_deps = [ "hilog:libhilog" ]
    }
    external_deps += [
      "drivers_interface_camera:metadata",
      "hitrace_native:hitrace_meter",
      "ipc:ipc_single",
    ]
    shlib_type = "hdi"
    public_configs = [ ":camhdi_impl_config" ]
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_camera"
  }

  ohos_static_library("camera_host_service_1.0_static") {
    sources = host_sources
    include_dirs = host_includes

    deps = [
      "$camera_path/buffer_manager:camera_buffer_manager",
      "$camera_path/device_manager:camera_device_manager",
      "$camera_path/metadata_manager:camera_metadata_manager",
      "$camera_path/pipeline_core:camera_pipeline_core",
      "$camera_path/utils:camera_utils",
      "//drivers/hdf_core/adapter/uhdf2/host:libhdf_host",
      "//drivers/interface/camera/v1_0:libcamera_stub_1.0",
    ]

    defines = []
    if (enable_camera_device_utest) {
      defines += [ "CAMERA_DEVICE_UTEST" ]
    }
    if (use_hitrace) {
      defines += [ "HITRACE_LOG_ENABLED" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "graphic_chipsetsdk:surface",
        "hdf_core:libhdf_host",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hitrace_native:hitrace_meter",
        "hiviewdfx_hilog_native:libhilog",
        "ipc:ipc_single",
      ]
      if (use_hitrace) {
        external_deps += [ "hitrace_native:libhitracechain" ]
      }
    } else {
      external_deps = [ "hilog:libhilog" ]
    }
    external_deps += [
      "drivers_interface_camera:metadata",
      "hitrace_native:hitrace_meter",
      "ipc:ipc_single",
    ]

    public_configs = [ ":camhdi_impl_config" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_camera"
  }
}
